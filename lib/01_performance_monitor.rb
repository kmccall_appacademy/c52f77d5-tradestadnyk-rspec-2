def measure(num=1, &block)
  start_time = Time.now
  num.times do
    block.call
  end
  (Time.now - start_time) / num
end
